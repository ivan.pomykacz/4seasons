# 4seasons

## Zadání

Vytvořte webovou stránku (stránky), kde každá stránka reprezentuje jedno roční období (z cyklu jaro/léto/podzim/zima).

  * Každá stránka obsahuje vycpávkový text, který je formátován skrze nadpisy a odstavce.
  * Na stránce bude alespoň jeden hlavní nadpis a několik odstavců.
  * Navrhněte CSS styly pro vytvoření look&feel jednotlivých stránek, které budou reprezentovat čtvero ročních období - zejména po barvevné stránce.
  * Využijte LESS, kde ve zvláštním souboru definujete barvy a v ostatních souborech (vždy jeden soubor pro jedno období) tyto barvy použijete (@include).
  * Stránka nemusí být repsponzivní, ale hlavní, obsahové, části nastavte odstazení zleva a zprava, tj. aby obsah nebyl zcela od kraje ke kraji.
  * Stránky budou obsahovat navigaci (navbar) na další období. Odkaz by měl reflektovat aktuální výběr.

## Instalace

Doplnit **kroky** k instalaci ...

## Autoři

  * Ivan Pomykacz



